const colors = require("colors"),
    mongo = require("mongodb")
module.exports = {
    console: {
        info(event, description, rl = null) {
            console.info(`[${event}]`.gray, `${new Date().toLocaleTimeString()}`.green, `${description}`.white)
        },
        warn(event, description) {
            console.warn(`[${event}]`.gray, `${new Date().toLocaleTimeString()}`.green, `${description}`.yellow)
        },
        error(event, description) {
            console.error(`[${event}]`.gray, `${new Date().toLocaleTimeString()}`.green, `${description}`.red)
        }
    },
    misc: {
        plural(word, amount) {
            return (amount > 1) ? `${word}s` : word
        },
        filename(string) {
            var name = string.split(".")
            let ext = name.pop()
            return {name: name.join("."), ext}
        }
    },
    database: {
        async set(namespace, key, item) {
            (await mongo.connect("mongodb://localhost/taco")).db("taco").collection(namespace).insertOne({documentKey: key, documentContent: item})
        },
        async get(namespace, key) {
            return ((await (await mongo.connect("mongodb://localhost/taco")).db("taco").collection(namespace).findOne({documentKey: key})) || {documentContent: {}})["documentContent"]
        }
    }
}
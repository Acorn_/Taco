module.exports.run = async (client, command) => {
    if (command.message.channel.type == "text") {
        let guildConfig = command.message.guild.config
        let hasPerms = (cmd) => {
            if (client.config.ownerID == command.message.author.id) return true;
            if (client.commands[cmd].info.permLevel >= 32) return false;
            if (command.message.guild.ownerID == command.message.author.id) return true;
            if (!guildConfig || !guildConfig.perms && client.commands[cmd].info.permLevel <= 1) return true; 
            if (!guildConfig.perms || !guildConfig.perms.commands) return false;
            if ((!guildConfig.perms.commands[cmd] || !client.commands[cmd].info.permLevel) || (guildConfig.perms.commands[cmd] || client.commands[cmd].info.permLevel) <= (guildConfig.perms.roles[cmd.message.guild.defaultRole] || 1)) return true; else {
                if ((guildConfig.perms.commands[cmd] || client.commands[cmd].info.permLevel) <= guildConfig.perms.users[cmd.message.author.id]) return true
                cmd.message.member.roles.forEach(r => {
                    if (guildConfig.perms.roles[r] >= (guildConfig.perms.commands[cmd] || client.commands[cmd].info.permLevel)) return true;
                })
            }
            return false
        }
        if (!command.args.command) {
            var cmdcatg = {}
            Object.keys(client.commands).forEach(c => {
                if (!hasPerms(c)) return;
                var args = ""
                if (client.commands[c].info.args) {
                    client.commands[c].info.args.forEach(r => {
                    if (r.optional) args = args + ` <${r.name}>`; else args = args + ` [${r.name}]`
                    }) 
                }
                if (client.commands[c].info.help.category) {
                    if (!cmdcatg[client.commands[c].info.help.category]) cmdcatg[client.commands[c].info.help.category] = [];
                    cmdcatg[client.commands[c].info.help.category].push(`${client.config.prefix + c}${args}`)
                } else {
                    if (!cmdcatg["Uncategorised"]) cmdcatg["Uncategorised"] = [];
                    cmdcatg["Uncategorised"].push(`${client.config.prefix + c}${args}`)
                }
            })
            var msg = ""
            Object.keys(cmdcatg).forEach(c => {
                msg = msg + `\n${c}:\n   ${cmdcatg[c].join("\n   ")}\n `
            })
            command.message.author.send(`\`\`\`\nTaco Help\n \n[required args] <optional args>\n ${msg}\`\`\``) 
        } else {
            if (client.commands[command.args.command]) {
                var args = ""
                if (client.commands[command.args.command].info.args) {
                    client.commands[command.args.command].info.args.forEach(r => {
                    if (r.optional) args = args + ` <${r.name}>`; else args = args + ` [${r.name}]`
                    }) 
                }
                var msg = `${client.config.prefix + command.args.command}${args} - ${client.commands[command.args.command].info.help.description}`
                command.message.author.send(`\`\`\`\nTaco Help\n \n[required args] <optional args>\n \n${msg}\n \n${client.commands[command.args.command].info.help.longDesc}${(client.commands[command.args.command].info.help.example) ? `\n \nExample: ${client.commands[command.args.command].info.help.example}` : ""}\`\`\``)
                command.message.channel.send("Check your DMs!") 
            } else {
                command.message.channel.send("That command does not exist.")
            }
        }
    }
    if (command.message.channel.type == "dm") {
        if (!command.args.command) {
            var cmdcatg = {}
            Object.keys(client.commands).forEach(c => {
                if (!client.commands[c].info.allowDMs) return;
                var args = ""
                if (client.commands[c].info.args) {
                    client.commands[c].info.args.forEach(r => {
                    if (r.optional) args = args + ` <${r.name}>`; else args = args + ` [${r.name}]`
                    }) 
                }
                if (client.commands[c].info.help.category) {
                    if (!cmdcatg[client.commands[c].info.help.category]) cmdcatg[client.commands[c].info.help.category] = [];
                    cmdcatg[client.commands[c].info.help.category].push(`${client.config.prefix + c}${args}`)
                } else {
                    if (!cmdcatg["Uncategorised"]) cmdcatg["Uncategorised"] = [];
                    cmdcatg["Uncategorised"].push(`${client.config.prefix + c}${args}`)
                }
            })
            var msg = ""
            Object.keys(cmdcatg).forEach(c => {
                msg = msg + `\n${c}:\n   ${cmdcatg[c].join("\n   ")}\n `
            })
            command.message.author.send(`\`\`\`\nTaco Help\n \n[required args] <optional args>\n ${msg}\`\`\``) 
        } else {
            if (client.commands[command.args.command]) {
                var args = ""
                if (client.commands[command.args.command].info.args) {
                    client.commands[command.args.command].info.args.forEach(r => {
                    if (r.optional) args = args + ` <${r.name}>`; else args = args + ` [${r.name}]`
                    }) 
                }
                var msg = `${client.config.prefix + command.args.command}${args} - ${client.commands[command.args.command].info.help.description}`
                command.message.author.send(`\`\`\`\nTaco Help\n \n[required args] <optional args>\n \n${msg}\n \n${client.commands[command.args.command].info.help.longDesc}${(client.commands[command.args.command].info.help.example) ? `\n \nExample: ${client.commands[command.args.command].info.help.example}` : ""}\`\`\``)
            } else {
                command.message.channel.send("That command does not exist.")
            }
        }
    }
}
module.exports.info = {
    permLevel: 0,
    args: [{name: "command", optional: true}],
    enabled: true,
    allowDMs: true,
    help: {
        category: "General",
        description: "Get a list of all commands you have access to.",
        longDesc: "List all commands that you can access or get more information about a command.",
        example: 't.help ping'
    }
}
module.exports.run = async (client, command) => {
    function convertMS(ms) {
        var d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        return { days: d, hours: h, minutes: m, seconds: s };
      };
    let upt = convertMS(client.uptime)
    let embed = new client.discord.MessageEmbed()
        .setTitle("Taco")
        .setDescription("Get some stats about Taco.")
        .addField("Servers", client.guilds.size, true)
        .addField("Members", client.users.size, true)
        .addField("Owner", `<@${client.config.ownerID}>`, true)
        .addField("Modules loaded", Object.keys(client.commands).length + Object.keys(client.events).length, true)
        .addField("Memory Usage", (process.memoryUsage().heapTotal / 1000 / 1000).toFixed(2) + "MB", true)
        .addField("Library", `[discord.js](https://discord.js.org/)`, true)
        .addField("Uptime", `${upt.days}d ${upt.hours}h ${upt.minutes}m ${upt.seconds}s`)
        .setThumbnail(client.user.avatarURL({format: "png", size: 128}))
    command.message.channel.send({embed})
}
module.exports.info = {
    allowDMs: true,
    permLevel: 0,
    help: {
        category: "General",
        description: "Get stats about Taco.",
        longDesc: "Get a kind-of-detailed list of Taco stats."
    }
}
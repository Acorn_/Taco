module.exports.run = async (client, command) => {
    command.message.channel.send({embed: {
        color: 0xfec73d,
        title: ":ping_pong: Pong!",
        description: `Ping time: ${Math.round((Date.now() - command.message.createdTimestamp) / 6)}ms`
    }})
}
module.exports.info = {
    permLevel: 0,
    args: [],
    enabled: true,
    allowDMs: true,
    help: {
        category: "General",
        description: "Check the response time of Taco.",
        longDesc: "Check the time that Taco needs to respond to commands."
    }
}
const jimp = require("jimp"),
    parseColor = require("parse-color")
module.exports.run = async (client, command) => {
    let rcolor = (command.args.color) ? (parseColor("#" + command.args.color.replace("#", "").replace("0x","")).hex).replace("#", "") : Math.random().toString(16).slice(2, 8)
    let ocolor = rcolor.toUpperCase()
    rcolor = "0x" + rcolor + "ff"
    var image = new jimp(128, 128, parseInt(rcolor), function (err, image) {
        image.getBuffer(jimp.MIME_PNG, (err, buffer) => {
            let embed = new client.discord.MessageEmbed()
                .attachFiles([{attachment: buffer, name: "color.png"}])
                .setImage("attachment://color.png")
                .setColor(ocolor)
                .setTitle(`#${ocolor}`)
            command.message.channel.send({embed})
        })
    })
}
module.exports.info = {
    permLevel: 0,
    args: [{name: "color", optional: true}],
    enabled: true,
    allowDMs: true,
    help: {
        category: "Fun",
        description: "Preview a HEX color.",
        longDesc: "Preview a HEX color.",
        example: 't.color #fff'
    }
}
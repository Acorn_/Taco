// Imports.
const Discord = require("discord.js"),
    fs = require("fs-extra"),
    colors = require("colors"),
    cJSON = require("circular-json"),
    mongo = require("mongodb"),
    redis = require("redis"),
    utils = require("./utils.js"),
    nodeUtil = require("util"),
    chokidar = require("chokidar"),
    eslint = new (require("eslint").Linter)(),
    readline = require("readline")

// Defining the client and attaching all extra variables and modules to it.
let client = new Discord.Client({disableEveryone: true})
client.db = mongo.connect("mongodb://localhost/taco")
client.cache = redis.createClient("redis://localhost/1")
client.commands = {}
client.commandAliases = {}
client.events = {}
client.utils = utils
client.json = cJSON
client.fs = fs
client.discord = Discord
client.login(fs.readFileSync("./token").toString())

// Handle ready event.
client.on("ready", () => {
    utils.console.info("Bot", `Logged in successfully. \n      Name: ${client.user.username}#${client.user.discriminator} \n      ID: ${client.user.id} \n      Guilds: ${client.guilds.size} \n      Members: ${client.users.size}`)
    client.user.setPresence({game: {name: `Taco! Serving ${client.guilds.size} ${utils.misc.plural("server", client.guilds.size)}.`}})
})

// Read events.
fs.readdirSync("./events").forEach(e => {
    client.events[utils.misc.filename(e).name] = require(`./events/${e}`)
})

// Attach events.
Object.keys(client.events).forEach(e => {
    utils.console.info("Loader", `Loaded '${e}.js' event handler.`)
    client.on(e, (...args) => {
        if (!client.events[e]) return
        client.events[e].run(client, ...args)
    })
})

// Add commands.
fs.readdirSync("./commands").forEach(e => {
    if (!require(`./commands/${e}`).run || !require(`./commands/${e}`).info) return
    var commandHandler = require(`./commands/${e}`)
    client.commands[utils.misc.filename(e).name] = commandHandler
    if (commandHandler.info.aliases) {
        commandHandler.info.aliases.forEach(r => {
            client.commandAliases[r] = utils.misc.filename(e).name
        })
        utils.console.info("Loader", `Loaded '${e}' command handler. Handles: ${utils.misc.filename(e).name}, ${commandHandler.info.aliases.join(", ")}`)
    } else {
        utils.console.info("Loader", `Loaded '${e}' command handler. Handles: ${utils.misc.filename(e).name}`)
    }
})

var rl = readline.createInterface(process.stdin, process.stdout)
rl.setPrompt("")
rl.on('line', (line) => {
    var args = line.trim().split(/ +/g)
    if (args[0] == "") {
        rl.prompt()
        return
    }
    switch(args[0]) {
        case "reload": {
            switch(args[1]) {
                case "command": {
                    if (!args[2]) {
                        console.log("Invaild prompt.")
                        rl.prompt()
                        return
                    } else {
                        if (!fs.existsSync(`./commands/${args[2]}.js`) && client.commands[args[2]]) {
                            delete require.cache[require.resolve(`./commands/${args[2]}.js`)];
                            delete client.commands[args[2]]
                            Object.keys(client.commandAliases).forEach(e => {
                                if (client.commandAliases[e] == args[2]) {
                                    delete client.commandAliases[e]
                                }
                            })
                            console.log("Unloaded command.")
                        } else if (fs.existsSync(`./commands/${args[2]}.js`) && !client.commands[args[2]]) {
                            client.commands[args[2]] = require(`./commands/${args[2]}.js`)
                            if (client.commands[args[2]].info.aliases) {
                                client.commands[args[2]].info.aliases.forEach(r => {
                                    client.commandAliases[r] = args[2]
                                })
                            }
                            console.log("Loaded command.")
                        } else if (fs.existsSync(`./commands/${args[2]}.js`) && client.commands[args[2]]) {
                            delete require.cache[require.resolve(`./commands/${args[2]}.js`)];
                            client.commands[args[2]] = require(`./commands/${args[2]}.js`)
                            Object.keys(client.commandAliases).forEach(e => {
                                if (client.commandAliases[e] == args[2]) {
                                    delete client.commandAliases[e]
                                }
                            })
                            if (client.commands[args[2]].info.aliases) {
                                client.commands[args[2]].info.aliases.forEach(r => {
                                    client.commandAliases[r] = args[2]
                                })
                            }
                            console.log("Reloaded command.")
                        } else {
                            console.log("Command not found.")
                        }
                        rl.prompt()
                        return
                    }
                    break
                }
                case "event": {
                    if (!args[2]) {
                        console.log("Invaild prompt.")
                        rl.prompt()
                        return
                    } else {
                        if (!fs.existsSync(`./events/${args[2]}.js`) && client.events[args[2]]) {
                            delete require.cache[require.resolve(`./events/${args[2]}.js`)];
                            delete client.events[args[2]]
                            console.log("Unloaded event.")
                        } else if (fs.existsSync(`./events/${args[2]}.js`) && !client.events[args[2]]) {
                            client.events[args[2]] = require(`./events/${args[2]}.js`)
                            client.on(args[2], (...args) => {
                                if (!client.events[args[2]]) return
                                client.events[args[2]].run(client, ...args)
                            })
                            console.log("Loaded event.")
                        } else if (fs.existsSync(`./events/${args[2]}.js`) && client.events[args[2]]) {
                            delete require.cache[require.resolve(`./events/${args[2]}.js`)];
                            client.events[args[2]] = require(`./events/${args[2]}.js`)
                            console.log("Reloaded event.")
                        } else {
                            console.log("Event not found.")
                        }
                        rl.prompt()
                        return
                    }
                    break
                }
                default: {
                    console.log("Invaild prompt.")
                    rl.prompt()
                    return
                    break
                }
            }
            break
        }
        case "": {
            rl.prompt()
            return
            break
        }
        default: {
            console.log("Invaild prompt.")
            rl.prompt()
            return
            break
        }
    }
    rl.prompt()
}).on("close", () => {
    process.exit(0)
})

// Handle message event and execute commands.
utils.database.get("internal", "tacoConfig").then(tacoConfig => {
    client.config = tacoConfig
    client.on("message", async (message) => {
        if (message.author.id == client.user.id) return;
        if (message.author.bot) return;
        if (message.channel.type == "text") {
            let guildConfig = await utils.database.get("guilds", message.guild.id)
            let args = (
                (message.content.startsWith(`<@${client.user.id}> `)
            ) ? message.content.slice(`<@${client.user.id}> `.length)
            : message.content.slice(
                (guildConfig && guildConfig.misc && guildConfig.misc.prefix) 
                ? guildConfig.misc.prefix.length 
                : tacoConfig.prefix.length)
            ).trim().split(/ +/g)
            let command = args.shift().toLowerCase()
            if (!(message.content.startsWith((guildConfig && guildConfig.misc && guildConfig.misc.prefix) ? guildConfig.misc.prefix : tacoConfig.prefix) || message.content.startsWith(`<@${client.user.id}> `))) return;
            message.guild.config = guildConfig
            let hasPerms = (command) => {
                if (tacoConfig.ownerID == message.author.id) return true;
                if (client.commands[command].info.permLevel >= 32) return false;
                if (message.guild.ownerID == message.author.id) return true;
                if (!guildConfig || !guildConfig.perms && client.commands[command].info.permLevel <= 1) return true; 
                if (!guildConfig.perms || !guildConfig.perms.commands) return false;
                if ((!guildConfig.perms.commands[command] || !client.commands[command].info.permLevel) || (guildConfig.perms.commands[command] || client.commands[command].info.permLevel) <= (guildConfig.perms.roles[message.guild.defaultRole] || 1)) return true; else {
                    if ((guildConfig.perms.commands[command] || client.commands[command].info.permLevel) <= guildConfig.perms.users[message.author.id]) return true
                    message.member.roles.forEach(r => {
                        if (guildConfig.perms.roles[r] >= (guildConfig.perms.commands[command] || client.commands[command].info.permLevel)) return true;
                    })
                }
                return false;
            }
            let hasAliasPerms = (command) => {
                if (tacoConfig.ownerID == message.author.id) return true;
                if (client.commands[client.commandAliases[command]].info.permLevel > 32) return false;
                if (message.guild.ownerID == message.author.id) return true;
                if (!guildConfig || !guildConfig.perms && client.commands[client.commandAliases[command]].info.permLevel <= 1) return true; 
                if (!guildConfig.perms || !guildConfig.perms.commands) return false;
                if (!guildConfig.perms.commands[client.commandAliases[command]] || guildConfig.perms.commands[client.commandAliases[command]] <= (guildConfig.perms.roles[message.guild.defaultRole] || 1) || !client.commands[client.commandAliases[command]].info.permLevel || client.commands[client.commandAliases[command]].info.permLevel <= 1) return true; else {
                    if ((guildConfig.perms.commands[client.commandAliases[command]] || client.commands[client.commandAliases[command]].info.permLevel) <= guildConfig.perms.users[message.author.id]) return true
                    message.member.roles.forEach(r => {
                        if (guildConfig.perms.roles[r] >= (guildConfig.perms.commands[client.commandAliases[command]] || client.commands[client.commandAliases[command]].info.permLevel)) return true;
                    })
                }
                return false;
            }
            if (client.commands[command] && ((guildConfig && guildConfig.commands && guildConfig.commands[command]) ? guildConfig.commands[command] == true : true)) {
                if (client.commands[command].info.enabled !== undefined && !client.commands[command].info.enabled) {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command is disabled.`
                    }})
                    return
                }
                if (hasPerms(command)) {
                    if (client.commands[command].info.args) {
                        var pargs = {}
                        var runCmd = true
                        client.commands[command].info.args.forEach((l, i) => {
                            if (l.optional) {
                                if (args[i]) pargs[l.name] = args[i]
                            } else {
                                if (args[i]) pargs[l.name] = args[i]; else {
                                    runCmd = false
                                    var argmsg = ""
                                    client.commands[command].info.args.forEach(r => {
                                        if (r.optional) argmsg = argmsg + ` <${r.name}>`; else argmsg = argmsg + ` [${r.name}]`
                                    })
                                    message.channel.send({embed: {
                                        color: 0xfec73d,
                                        title: "Oops!",
                                        description: `Missing arguments. Command usage: ${tacoConfig.prefix + client.CommandAliases[command]} ${argmsg}`
                                    }})
                                    return
                                }
                            }
                        })
                        if (runCmd) client.commands[command].run(client, {message, args: pargs, rargs: args, command});
                    } else {
                        client.commands[command].run(client, {message, args: {}, rargs: args, command})
                    }
                } else {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `You are not permitted to do that.`
                    }})
                }
            } else if (client.commands[client.commandAliases[command]] && ((guildConfig && guildConfig.commands && guildConfig.commands[client.commandAliases[command]]) ? guildConfig.commands[client.commandAliases[command]] == true : true)) {
                if (client.commands[client.commandAliases[command]].info.enabled !== undefined && !client.commands[client.commandAliases[command]].info.enabled) {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command is disabled.`
                    }})
                    return
                }
                if (hasAliasPerms(command)) {
                    if (client.commands[client.CommandAliases[command]].info.args) {
                        var pargs = {}
                        var runCmd = true
                        client.commands[client.CommandAliases[command]].info.args.forEach((l, i) => {
                            if (l.optional) {
                                if (args[i]) pargs[l.name] = args[i]
                            } else {
                                if (args[i]) pargs[l.name] = args[i]; else {
                                    runCmd = false
                                    var argmsg = ""
                                    client.commands[client.CommandAliases[command]].info.args.forEach(r => {
                                        if (r.optional) argmsg = argmsg + ` <${r.name}>`; else argmsg = argmsg + ` [${r.name}]`
                                    })
                                    message.channel.send({embed: {
                                        color: 0xfec73d,
                                        title: "Oops!",
                                        description: `Missing arguments. Command usage: ${tacoConfig.prefix + client.CommandAliases[command]} ${argmsg}`
                                    }})
                                    return
                                }
                            }
                        })
                        if (runCmd) client.commands[client.CommandAliases[command]].run(client, {message, args: pargs, rargs: args, command});
                    } else {
                        client.commands[client.CommandAliases[command]].run(client, {message, args: {}, rargs: args, command})
                    }
                } else {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `You are not permitted to do that.`
                    }})
                }
            } else {
                if (guildConfig.misc && guildConfig.misc.invaildCommandMessages) {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command does not exist or it was disabled by the server owner.`
                    }})
                }
            }
        }
        if (message.channel.type == "dm") {
            let args = message.content.trim().split(/ +/g),
                command = args.shift().toLowerCase()
            if (client.commands[command]) {
                if (client.commands[command].info.enabled !== undefined && !client.commands[command].info.enabled) {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command is disabled.`
                    }})
                    return
                }
                if (client.commands[command].info.allowDMs) {
                    if (client.commands[command].info.args) {
                        var pargs = {}
                        var runCmd = true
                        client.commands[command].info.args.forEach((l, i) => {
                            if (l.optional) {
                                if (args[i]) pargs[l.name] = args[i]
                            } else {
                                if (args[i]) pargs[l.name] = args[i]; else {
                                    runCmd = false
                                    var argmsg = ""
                                    client.commands[command].info.args.forEach(r => {
                                        if (r.optional) argmsg = argmsg + ` <${r.name}>`; else argmsg = argmsg + ` [${r.name}]`
                                    })
                                    message.channel.send({embed: {
                                        color: 0xfec73d,
                                        title: "Oops!",
                                        description: `Missing arguments. Command usage: ${tacoConfig.prefix + client.CommandAliases[command]} ${argmsg}`
                                    }})
                                    return
                                }
                            }
                        })
                        if (runCmd) client.commands[command].run(client, {message, args: pargs, rargs: args, command});
                    } else {
                        client.commands[command].run(client, {message, args: {}, rargs: args, command})
                    }
                } else {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command does not work in DMs.`
                    }})
                }
            } else if (client.commands[client.commandAliases[command]]) {
                if (client.commands[client.commandAliases[command]].info.enabled !== undefined && !client.commands[client.commandAliases[command]].info.enabled) {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command is disabled.`
                    }})
                    return
                }
                if (client.commands[client.commandAliases[command]].info.allowDMs) {
                    if (client.commands[client.CommandAliases[command]].info.args) {
                        var pargs = {}
                        var runCmd = true
                        client.commands[client.CommandAliases[command]].info.args.forEach((l, i) => {
                            if (l.optional) {
                                if (args[i]) pargs[l.name] = args[i]
                            } else {
                                if (args[i]) pargs[l.name] = args[i]; else {
                                    runCmd = false
                                    var argmsg = ""
                                    client.commands[client.CommandAliases[command]].info.args.forEach(r => {
                                        if (r.optional) argmsg = argmsg + ` <${r.name}>`; else argmsg = argmsg + ` [${r.name}]`
                                    })
                                    message.channel.send({embed: {
                                        color: 0xfec73d,
                                        title: "Oops!",
                                        description: `Missing arguments. Command usage: ${tacoConfig.prefix + client.CommandAliases[command]} ${argmsg}`
                                    }})
                                    return
                                }
                            }
                        })
                        if (runCmd) client.commands[client.CommandAliases[command]].run(client, {message, args: pargs, rargs: args, command});
                    } else {
                        client.commands[client.CommandAliases[command]].run(client, {message, args: {}, rargs: args, command})
                    }
                } else {
                    message.channel.send({embed: {
                        color: 0xfec73d,
                        title: "Oops!",
                        description: `That command does not work in DMs.`
                    }})
                }
            } else {
                message.channel.send({embed: {
                    color: 0xfec73d,
                    title: "Oops!",
                    description: `That command does not exist.`
                }})
            }
        }
    })
})